import org.apache.commons.codec.digest.DigestUtils;

public class ComparerSha256 extends Comparer{
	String digest;
	public String compare(String Digestable) {
		digest = DigestUtils.sha256Hex(Digestable);
		return digest;
	}
}
