import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class StringGen {
	static String letterlist = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789@#!&$%?[]{}/;:<>*";
	static String[] letterarray = new String[1];
	static int n = letterarray.length -1;
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Paste in a hash");
		String hash = reader.readLine();
		System.out.println("What type of hash is it?");
		System.out.println("Enter 1 for sha256, 2 for sha512, and 3 for MD5");
		String hashtypefinder = reader.readLine();
		int hashtype = Integer.parseInt(hashtypefinder);
		Comparer Comparer = null;
		if(hashtype == 1) {
			Comparer = new ComparerSha256();
		}
		else if(hashtype == 2) {
			Comparer = new ComparerSha512();
		}
		else if(hashtype == 3) {
			Comparer = new ComparerMD5();
		}
		while(true) {
		for(short i = 0; i<letterlist.length();i++) {
			letterarray[n] = Character.toString(letterlist.charAt(i));
			while(letterarray[n].equals("*") && n!=0) {
				letterarray[n] = Character.toString(letterlist.charAt(0));
				n--;					
				if(!"*".equals(letterarray[n])) {
					int location = letterlist.indexOf(letterarray[n]);
					letterarray[n] = Character.toString(letterlist.charAt(location + 1));
					n = letterarray.length -1;
				}
				}
			if(n == 0 && letterarray[n].equals("*")) {
				letterarray[n] = Character.toString(letterlist.charAt(0)); 
				int length = letterarray.length + 1;
				String[] letterarray2 = new String[length];
				letterarray = letterarray2;		
				n = letterarray.length -1;
				int n2 = letterarray.length -1;
				while(n2 > -1) {
						letterarray[n2] = Character.toString(letterlist.charAt(0));
						n2--;
						}
			}
		StringBuilder finalstring = new StringBuilder();
		for(short p = 0; p < letterarray.length; p++) {
			  finalstring.append(letterarray[p]);
		}
		String digested = Comparer.compare(finalstring.toString());
		if(digested.equals(hash)) {
			System.out.println("I've found the password! It is " + finalstring +".");
			System.exit(0);
		}
		}
		}
		} 
}