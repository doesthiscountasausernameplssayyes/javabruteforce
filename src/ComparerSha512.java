import org.apache.commons.codec.digest.DigestUtils;

public class ComparerSha512 extends Comparer{
	String digest;
	public String compare(String Digestable) {
		digest = DigestUtils.sha512Hex(Digestable);
		return digest;
	}
}
